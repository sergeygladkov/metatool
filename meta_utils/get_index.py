# -*- coding: utf-8 -*-g
"""Модуль для получения информации из .idx файлов istream"""

from struct import *
import collections
import sys
import os


class RecordingChunkInfoHeader:
    """класс, для получения данных по чанку."""

    def __decode_idx_block(self):
        """функция для получения блока данных"""
        structFormat = Struct('=BBBQQQQiiQQQQH')
        self.data = self.idx_file.read(structFormat.size)
        self.unpacked = structFormat.unpack(self.data)
        idx_struct = {
            "entrySize": self.unpacked[0],
            "blockType": self.unpacked[1],
            "streamType": self.unpacked[2],
            "streamId": self.unpacked[3],
            "flags": self.unpacked[4],
            "duration": self.unpacked[5],
            "timestamp": self.unpacked[6],
            "ts_rel": self.unpacked[7],
            "dts_rel": self.unpacked[8],
            "size": self.unpacked[9],
            "offset": self.unpacked[10],
            "index": self.unpacked[11],
            "blockId": self.unpacked[12],
            "mark": self.unpacked[13]
        }
        return idx_struct

    def get_file_length(self):
        """получаем размер файла."""
        return os.stat(self.idx_file.name).st_size

    def check_idx_length_min_one_block(self):
        """проверяем idx, что имеет минимум один блок"""
        if self.get_file_length() < 77:
            print("\n IDX file {number} is empty! ".format(number=str(self.path)))
            sys.exit(100)
        else:
            return True

    def check_idx_met_incorrect_data(self, idx_struct):
        """проверяем, что idx файл содержит нечитаемые данные"""
        if idx_struct["blockId"] == 0 and idx_struct["entrySize"] == 0:
            return True

    def _read_file_and_decode_to_list(self):
        """декодируем бинарник в список json блоков"""
        self.check_idx_length_min_one_block()
        self.file_length = self.get_file_length()

        block_read = 0
        while block_read < self.file_length:
            idx_struct = self.__decode_idx_block()

            if self.check_idx_met_incorrect_data(idx_struct=idx_struct):
                if block_read == 0:
                    print("\nIDX file {number} has incorrect data".format(number=str(self.path)))
                    sys.exit(100)
                else:
                    break

            self.block_list.append(idx_struct)
            block_read = block_read + 77

        self.idx_file.close()
        return self.block_list

    def __init__(self, idx_path):
        self.block_list = []
        self.path = idx_path
        self.idx_file = open(self.path, 'rb')
        self._read_file_and_decode_to_list()

    def get_last_sps_timestamp(self):
        """ ищем  последний вхождение sps-timestamp."""
        for block in reversed(self.block_list):
            if block["blockType"] == 7:
                return block["timestamp"]

    def get_first_sps_timestamp(self):
        """ищем первое sps-timestamp"""
        for block in self.block_list:
            if block["blockType"] == 7:
                return block["timestamp"]

    def get_duration_chunk(self):
        """продолжительность чанка"""
        chunk_duration = 0
        for block in self.block_list:
            if block["mark"] != 0xbabe:
                break
            if (int(block["flags"]) & 2) > 0:
                chunk_duration = chunk_duration + block["duration"]

        return chunk_duration

    def get_index_chunk(self):
        """получаем индекс чанка"""
        return self.block_list[1]["index"]

    def get_offset_chunk(self):
        """оффсет относительно текущего файла"""
        return self.block_list[-1]["offset"]

    def get_last_blockid(self):
        """получаем id последнего блока"""
        return self.block_list[-1]["blockId"]

    def result_find_idx_elements(self):
        """получаем словарь с выгрузкой данных для метадаты из idx файла."""
        dict_element = collections.OrderedDict([
            ("duration", self.get_duration_chunk()),
            ("index", self.get_index_chunk()),
            ("offset", self.get_offset_chunk()),
            ("R0", 0),
            ("R1", 0),
            ("firstBlockTimestamp", self.get_first_sps_timestamp()),
            ("lastBlockTimestamp", self.get_last_sps_timestamp()),
            ("lastBlockId", self.get_last_blockid())
        ])
        return dict_element

