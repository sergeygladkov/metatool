# -*- coding: utf-8 -*-
"""Основной скрипт, для запуска утилиты из командной строки"""
import argparse
from meta_utils.wr_metadata import RecordingMetaDataHeader, write_meta_from_json
import struct
import json
import time
from meta_utils import get_meta
import sys

version = '0.4'


class CheckEndSolidus(argparse.Action):
    """проверка, в конце пути к директории на слэш '/' """

    def __init__(self, option_strings, dest, nargs=None, **kwargs):
        if nargs is not None:
            raise ValueError("nargs not allowed")
        super(CheckEndSolidus, self).__init__(option_strings, dest, **kwargs)

    def __call__(self, parser, namespace, values, option_string=None):
        if values[-1] != "/":
            values = values + "/"
        else:
            values = values
        setattr(namespace, self.dest, values)


def get_stream_info(metadata_file=None, output_file=None, channel_dir=None):
    """Get STREAM information, from METADATA"""

    if metadata_file:
        metadata = metadata_file
    else:
        metadata = channel_dir + 'metadata'

    with open(metadata, "rb") as meta:
        metadata_header = get_meta.read_metadata_header(meta)

        info_each_chunk = get_meta.meta_info_chunk(meta, metadata_header)
        elementary_flow = get_meta.elementary_flow(meta)

        if not elementary_flow:
            print("No elementary stream in metadata.")
            sys.exit(100)

        if output_file:
            with open(output_file, "w") as stream_file:
                for each_flow in elementary_flow:
                    stream_file.write(each_flow)
        else:
            return elementary_flow

        get_meta.chunks_path_list(metadata_header["chunksCount"] * 2, meta)


def write_header(metadata, meta_write, idx_block_info):
    """запись заголовочной информации"""
    meta_write.write(struct.pack('Q', metadata.get_first_index_in_channel()))
    for key_idx_block in idx_block_info:
        meta_write.write(struct.pack('Q', metadata.get_last_chunk_meta()[key_idx_block]))
    meta_write.write(struct.pack('Q', metadata.get_total_duration()))
    meta_write.write(struct.pack('Q', metadata.get_chunks_count()))


def write_idx(metadata, meta_write, idx_block_info):
    """запись по всем чанкам."""
    idx_list_meta = metadata.list_idx_block
    for chunk_id in range(metadata.get_chunks_count()):
        for key_idx_block in idx_block_info:
            meta_write.write(struct.pack('Q', idx_list_meta[chunk_id][key_idx_block]))


def write_elem_stream(metadata, meta_write, video_elementary):
    """ Запись информации о элементарном потоке"""
    if isinstance(video_elementary, int):
        meta_write.write(struct.pack('Q', video_elementary))
    else:
        streamId = 0
        streamInfoCount = len(video_elementary)
        meta_write.write(struct.pack('Q', streamInfoCount))
        for json_path in video_elementary:

            if isinstance(json_path, unicode):
                json_elementary = json_path
            else:
                json_elementary = metadata.get_elementary_stream(json_path)
            streamType = ''
            ElementaryStreamEncodingType = {"h264": 1,

                                            "g711": 2,
                                            "aac": 3,
                                            "archive_config": 4,
                                            "mjpeg": 5}

            for encoding_type in ElementaryStreamEncodingType.keys():
                if encoding_type == json.loads(json_elementary)["stream_traits"]["encoding"]:
                    streamType = ElementaryStreamEncodingType[encoding_type]

            size_json = len(json_elementary)

            meta_write.write(struct.pack('Q', streamId))
            meta_write.write(struct.pack('Q', streamType))
            meta_write.write(struct.pack('Q', size_json))

            for i in range(size_json):
                meta_write.write(struct.pack('c', json_elementary[i].encode()))

            streamId = streamId + 2

def write_path_chunk(metadata, meta_write):
    """ запись информации о чанках"""
    chanks_paths = metadata.get_all_chunk_path()

    chunk_idx = chanks_paths.pop()
    chunk_data = chanks_paths.pop()
    meta_write.write(struct.pack('Q', len(chunk_data)))
    for char in range(len(chunk_data)):
        meta_write.write(struct.pack('c', chunk_data[char].encode()))

    meta_write.write(struct.pack('Q', len(chunk_idx)))
    for char in range(len(chunk_idx)):
        meta_write.write(struct.pack('c', chunk_idx[char].encode()))

    for chunk in chanks_paths:
        meta_write.write(struct.pack('Q', len(chunk)))
        for char in range(len(chunk)):
            meta_write.write(struct.pack('c', chunk[char].encode()))


def main():
    start = time.time()

    idx_block_info = ["duration", "index", "offset", "R0", "R1", "firstBlockTimestamp", "lastBlockTimestamp",
                      "lastBlockId"]

    parser = argparse.ArgumentParser(description="This is help, for work with metadata.")
    parser.add_argument("-v", "--version", action='version', version='%(prog)s {ver}'.format(ver=version))
    parser.add_argument("-d", "--dir", action=CheckEndSolidus,
                        help="Path to dir with channel: chunks and idx. "
                             "Create metadata without stream_json in."
                             "If need with stream_json for ECHD, use Json or Metadata")
    parser.add_argument("-j", "--json", nargs="*",
                        help="Аргумент, для добовления json с элементарными потоками, "
                             "можно перечислить несколько json.")
    parser.add_argument("-r", "--read", help="Path to metadata, for read")
    parser.add_argument('-s', '--stream',
                        help="Get stream json in file(need put path), from metadata, use only with '-r'")
    parser.add_argument('-m', '--metadata', help="Use old metadata to get new metadata 'metadata_new', use with -d")
    parser.add_argument('-wj', '--writejson', help="Write json in file from path old metadata")
    parser.add_argument('-w', '--writemeta', help="Use json file (put path) to get new metadata 'metadata_new' ")
    args = parser.parse_args()

    if args.dir and args.json or args.metadata:
        metadata = RecordingMetaDataHeader(args.dir)

        if args.json:
            video_elementary = args.json
        else:
            video_elementary = get_stream_info(channel_dir=args.dir, metadata_file=args.metadata)

        with open('metadata_new', 'wb') as meta_write:

            write_header(metadata, meta_write, idx_block_info)
            write_idx(metadata, meta_write, idx_block_info)
            write_elem_stream(metadata, meta_write, video_elementary)
            write_path_chunk(metadata, meta_write)

        end = time.time()
        print("\nMetadata created!  Time elapsed:", end - start, "\n")

    elif args.dir:
        metadata = RecordingMetaDataHeader(args.dir)

        with open('metadata_new', 'wb') as meta_write:

            write_header(metadata, meta_write, idx_block_info)
            write_idx(metadata, meta_write, idx_block_info)
            write_elem_stream(metadata, meta_write, video_elementary=0)
            write_path_chunk(metadata, meta_write)

        end = time.time()
        print("\nMetadata created without ElementaryStream! NEW METADATA  Time elapsed:", end - start, "\n")

    elif args.read and args.stream:
        get_stream_info(metadata_file=args.read, output_file=args.stream)
        print("create streams from metadata")

    elif args.writejson:
        with open('metadata.json', 'w') as outfile:
            json.dump(get_meta.create_metadata_json(args.writejson), outfile)
        print("DONE: json write in file metadata.json")

    elif args.read:
        print(json.dumps(get_meta.create_metadata_json(args.read), indent=2))

    elif args.writemeta:
        write_meta_from_json(args.writemeta)
        print("CREATE NEW METADATA from json")


if __name__ == "__main__":
    main()

