# -*- coding: utf-8 -*-
"""Модуль для чтения файла metadata"""

from struct import *
import json
import collections


def read_metadata_header(meta):
    """читаем заголовок metadata"""
    first_index = unpack("Q", meta.read(8))[0]

    recording_chunk_info_header = collections.OrderedDict([
        ("duration", unpack("Q", meta.read(8))[0]),
        ("index", unpack("Q", meta.read(8))[0]),
        ("offset", unpack("Q", meta.read(8))[0]),
        ("R0", unpack("Q", meta.read(8))[0]),
        ("R1", unpack("Q", meta.read(8))[0]),
        ("firstBlockTimestamp", unpack("Q", meta.read(8))[0]),
        ("lastBlockTimestamp", unpack("Q", meta.read(8))[0]),
        ("lastBlockId", unpack("Q", meta.read(8))[0]),
    ])

    recording_meta_data_header = collections.OrderedDict([
        ("firstIndex", first_index),
        ("lastChunkHeader", recording_chunk_info_header),
        ("totalDuration", unpack("Q", meta.read(8))[0]),
        ("chunksCount", unpack("Q", meta.read(8))[0])
    ])

    return recording_meta_data_header


def meta_info_chunk(meta, metadata_header):
    """получаем мета данные по всем чанкам"""
    list_chunks = []
    i = 0

    while i < metadata_header["chunksCount"]:
        recording_chunk_info_header = collections.OrderedDict([
            ("duration", unpack("Q", meta.read(8))[0]),
            ("index", unpack("Q", meta.read(8))[0]),
            ("offset", unpack("Q", meta.read(8))[0]),
            ("R0", unpack("Q", meta.read(8))[0]),
            ("R1", unpack("Q", meta.read(8))[0]),
            ("firstBlockTimestamp", unpack("Q", meta.read(8))[0]),
            ("lastBlockTimestamp", unpack("Q", meta.read(8))[0]),
            ("lastBlockId", unpack("Q", meta.read(8))[0]),
        ])

        list_chunks.append(recording_chunk_info_header)
        i += 1

    return list_chunks


def stream_info_json(count, stream_info_count, meta):
    """Получаем размер json и доп. информацию"""
    # if count == 0:
    stream_info = {"streamInfoCount": stream_info_count}
    # else:
        # stream_info = {"streamInfoCount": unpack("Q", meta.read(8))[0]}

    stream_info.update({
        "streamId": unpack("Q", meta.read(8))[0],
        "streamType": unpack("Q", meta.read(8))[0],
        "size_json": unpack("Q", meta.read(8))[0],
    })
    return stream_info


def stream_json(size, meta):
    """получаем по потоку, json"""
    json_element = ''
    for each_char in range(size):
        json_element = json_element + unpack("c", meta.read(1))[0].decode("utf-8")

    return json_element


def elementary_flow(meta):
    """получаем элементарные потоки"""
    stream_info_count = unpack("Q", meta.read(8))[0]
    elementary_list = []

    if stream_info_count > 0:
        for count in range(stream_info_count):
            stream_size = stream_info_json(count, stream_info_count, meta)["size_json"]
            elementary_list.append(stream_json(stream_size, meta))

    return elementary_list


def chunks_path_list(chunk_count, meta):
    """функция пути до чанков."""
    chunks = []
    for i in range(chunk_count):
        path_length = unpack("Q", meta.read(8))[0]
        path = ''
        for x in range(path_length):
            path += unpack("c", meta.read(1))[0].decode("utf-8")

        chunks.append(path)

    return chunks


def create_metadata_json(metadata_path):
    """создаём json из metadata, вызывая другие функции и добовляя содержимое в dict."""
    meta_json = {}
    with open(metadata_path, "rb") as meta_file:
        metadata_header = read_metadata_header(meta_file)
        meta_json["HEADER"] = metadata_header
        meta_json["INFO_CHUNK"] = meta_info_chunk(meta_file, metadata_header)
        meta_json["ELEMENTARY"] = elementary_flow(meta_file)
        meta_json["CHUNK_PATH"] = chunks_path_list((metadata_header["chunksCount"] + 1) * 2, meta_file)

    return meta_json
