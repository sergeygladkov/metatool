# -*- coding: utf-8 -*-
"""Создание метада из idx файлов"""
import os
from meta_utils.get_index import RecordingChunkInfoHeader
import sys
import struct
import json


class RecordingMetaDataHeader(RecordingChunkInfoHeader):
    """класс, для получения всех необходимых элементов для записи в метадату по каналу."""

    def get_list_chunk_idx(self):
        """получаем список всех idx и data файлов"""
        list_chunk = []
        for each_chunk in sorted(os.listdir(self.PATH_CHANNEL)):
            if each_chunk.endswith(".idx") or each_chunk.endswith(".data"):
                list_chunk.append(each_chunk)

        return list_chunk

    def __init__(self, path_dir_channel):
        """инициализируем переменные."""
        self.PATH_CHANNEL = path_dir_channel
        self.list_name_chunk = self.get_list_chunk_idx()
        self.list_idx_block = []
        self.__get_idx_list_blocks()

    def get_idx_file_in_channel(self):
        """Выборка idx файлов, из списка всех файлов в директории канала"""
        list_idx_file = []
        for filename_chunk in self.list_name_chunk:
            if filename_chunk.endswith(".idx"):
                list_idx_file.append(filename_chunk)

        return list_idx_file

    def __get_idx_list_blocks(self):
        """ОБЯЗАТЕЛЬНО! получаем список с результатом по каждому idx файлу"""
        for each_idx in self.get_idx_file_in_channel():
            idx_metadata = RecordingChunkInfoHeader(self.PATH_CHANNEL + each_idx)
            self.list_idx_block.append(idx_metadata.result_find_idx_elements())

        return self.list_idx_block

    def get_total_duration(self):
        """Получение, общей длинны канала"""
        duration_channel = 0
        if self.get_idx_file_in_channel().__len__() != self.get_chunks_count() + 1:
            print("""The number of chunks between last and first idx file
                does not correspond to the number of idx files in channel.
                Number of chunks that are missing: {numb} """.format(
                numb=self.get_chunks_count() - self.get_idx_file_in_channel().__len__()))
            sys.exit(100)
        for chunk_id in self.list_idx_block[:-1]:
            duration_channel += chunk_id["duration"]

        return duration_channel

    def get_first_index_in_channel(self):
        """Получаем первый индекс в канале"""
        return self.list_idx_block[0]["index"]

    def get_last_index_in_channel(self):
        """получаем последний индекс в канале"""
        return self.list_idx_block[-1]["index"]

    def get_chunks_count(self):
        """Получаем количество чанков в канале"""
        return self.get_last_index_in_channel() - self.get_first_index_in_channel()

    def get_last_chunk_meta(self):
        """получаем metadata по последнему чанку в канале"""
        return self.list_idx_block[-1]

    def get_elementary_stream(self, path):
        """получаем элементарный поток из json файлов, переданных через ключ."""
        with open(path, 'r') as json_elementary:
            return json_elementary.read()

    def get_all_chunk_path(self):
        """получаем список путей, всх чанков."""
        list_absolute_path_chunk = []
        for filename_chunk in self.list_name_chunk:
            if filename_chunk.startswith("recording."):
                list_absolute_path_chunk.append(self.PATH_CHANNEL + filename_chunk)

        return list_absolute_path_chunk


def write_meta_from_json(file_json_from_meta):
    """функция, для записи чанков из json."""
    idx_block_info = ["duration", "index", "offset", "R0", "R1",
                      "firstBlockTimestamp", "lastBlockTimestamp", "lastBlockId"]

    with open('metadata_new_json', 'wb') as meta_write:
        with open(file_json_from_meta, 'r') as file_json:
            metadata_json = json.load(file_json)
            # запись заголовочной информации
            meta_write.write(struct.pack('Q', metadata_json["HEADER"]["firstIndex"]))
            for key_idx_block in idx_block_info:
                meta_write.write(struct.pack('Q', metadata_json["HEADER"]["lastChunkHeader"][key_idx_block]))
            meta_write.write(struct.pack('Q', metadata_json["HEADER"]["totalDuration"]))
            meta_write.write(struct.pack('Q', metadata_json["HEADER"]["chunksCount"]))

            # запись по всем чанкам.
            for chunk_idx in metadata_json["INFO_CHUNK"]:
                for key_idx_block in idx_block_info:
                    meta_write.write(struct.pack('Q', chunk_idx[key_idx_block]))

            # Запись информации о элементарном потоке
            if metadata_json["ELEMENTARY"]:
                # for json_path in metadata_json["ELEMENTARY"]:
                print("Мы нашли элементарные потоки в JSON, но пока не обрабатываем их ни как.")
            else:
                meta_write.write(struct.pack('Q', 0))

            # запись информации о чанках
            for chunk_path in metadata_json["CHUNK_PATH"]:
                meta_write.write(struct.pack('Q', len(chunk_path)))
                for character in range(len(chunk_path)):
                    meta_write.write(struct.pack('c', chunk_path[character].encode()))

