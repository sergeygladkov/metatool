from setuptools import setup

setup(
    name='meta_utils',
    version="0.4",
    description='Utils for Istream3, work with metadata',
    author='Sergey Gladkov',
    author_email='s.gladkov@netris.ru',
    url='git@bitbucket.org:sergeygladkov/metadata_istream.git',
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 2.6',
    ],
    packages=['meta_utils'],
    entry_points={
        'console_scripts': [
            'meta_util = meta_utils.run:main'
        ]
    },
)
